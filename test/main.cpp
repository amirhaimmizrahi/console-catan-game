#include "game/game.h"

int main()
{
    Game game;

    while (true)
    {
        game.print();
        game.input_handler();
    }

    return 0;
}