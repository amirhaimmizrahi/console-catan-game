#include "game.h"

Game::Game()
{
  init_ncurses();
}

void Game::init_ncurses()
{
  setlocale(LC_ALL, "");
  initscr();
  noecho();
  cbreak();
  keypad(stdscr, true);

  m_board.init_colors();
}

void Game::terminate_ncurses()
{
  endwin();
  exit(0);
}

void Game::print()
{
  m_board.print();
  move(0, 0);
}

void Game::input_handler()
{
  switch (getch())
  {
  case KEY_UP:
    m_board.move_up();
    break;

  case KEY_DOWN:
    m_board.move_down();
    break;

  case KEY_LEFT:
    m_board.move_left();
    break;

  case KEY_RIGHT:
    m_board.move_right();
    break;

  case 'e':
    m_board.build_city();
    break;

  case 'q':
    terminate_ncurses();
  }
}
