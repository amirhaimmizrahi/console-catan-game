#ifndef GAME_H
#define GAME_H

#include <stdlib.h>
#include <locale.h>
#include "cell/cell.h"
#include "board/board.h"

class Game
{
public:
    Game();
    void print();
    void input_handler();

private:
    void init_ncurses();
    void terminate_ncurses();

    Board m_board;
};
#endif
