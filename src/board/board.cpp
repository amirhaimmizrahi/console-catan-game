#include "board.h"

Board::Board() : cursor_x(0), cursor_y(0)
{
    m_board[0] = {{-3, -1, Cell::WATER}, {-3, 0, Cell::WATER}, {-3, 1, Cell::WATER}, {-3, 2, Cell::WATER}};
    m_board[1] = {{-2, -2, Cell::WATER}, {-2, -1, Cell::WOOD}, {-2, 0, Cell::WOOD}, {-2, 1, Cell::WOOD}, {-2, 2, Cell::WATER}};
    m_board[2] = {{-1, -2, Cell::WATER}, {-1, -1, Cell::WOOL}, {-1, 0, Cell::WOOL}, {-1, 1, Cell::WOOL}, {-1, 2, Cell::WOOL}, {-1, 3, Cell::WATER}};
    m_board[3] = {{0, -3, Cell::WATER}, {0, -2, Cell::IRON}, {0, -1, Cell::IRON}, {0, 0, Cell::IRON}, {0, 1, Cell::IRON}, {0, 2, Cell::IRON}, {0, 3, Cell::WATER}};
    m_board[4] = {{1, -2, Cell::WATER}, {1, -1, Cell::WALL}, {1, 0, Cell::WALL}, {1, 1, Cell::WALL}, {1, 2, Cell::WALL}, {1, 3, Cell::WATER}};
    m_board[5] = {{2, -2, Cell::WATER}, {2, -1, Cell::FOOD}, {2, 0, Cell::FOOD}, {2, 1, Cell::FOOD}, {2, 2, Cell::WATER}};
    m_board[6] = {{3, -1, Cell::WATER}, {3, 0, Cell::WATER}, {3, 1, Cell::WATER}, {3, 2, Cell::WATER}};

    m_edges[0] = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}};
    m_edges[1] = {{1, 0}, {1, 1}, {1, 2}, {1, 3}, {1, 4}, {1, 5}, {1, 6}, {1, 7}, {1, 8}};
    m_edges[2] = {{2, 0}, {2, 1}, {2, 2}, {2, 3}, {2, 4}, {2, 5}, {2, 6}, {2, 7}, {2, 8}, {2, 9}, {2, 10}};
    m_edges[3] = {{3, 0}, {3, 1}, {3, 2}, {3, 3}, {3, 4}, {3, 5}, {3, 6}, {3, 7}, {3, 8}, {3, 9}, {3, 10}};
    m_edges[4] = {{4, 0}, {4, 1}, {4, 2}, {4, 3}, {4, 4}, {4, 5}, {4, 6}, {4, 7}, {4, 8}};
    m_edges[5] = {{5, 0}, {5, 1}, {5, 2}, {5, 3}, {5, 4}, {5, 5}, {5, 6}};
}

void Board::init_colors()
{
    start_color();

    init_pair(COLOR_HOVERED, COLOR_RED, COLOR_BLACK);
    init_pair(COLOR_DEFAULT, COLOR_WHITE, COLOR_BLACK);

    init_pair(COLOR_IRON, COLOR_BLACK, 8);
    init_pair(COLOR_WALL, COLOR_BLACK, COLOR_RED);
    init_pair(COLOR_FOOD, COLOR_BLACK, COLOR_YELLOW);
    init_pair(COLOR_WOOD, COLOR_BLACK, COLOR_GREEN);
    init_pair(COLOR_WOOL, COLOR_BLACK, COLOR_WHITE);

    init_pair(COLOR_WATER, COLOR_CYAN, COLOR_CYAN);
}

void Board::move_up()
{
    // if (get_cell(cursor_x, cursor_y - 1).material == Cell::WATER)
    //     return;

    cursor_y -= 1;
}

void Board::move_down()
{
    // if (get_cell(cursor_x, cursor_y + 1).material == Cell::WATER)
    //     return;

    cursor_y += 1;
}

void Board::move_right()
{
    // if (get_cell(cursor_x + 1, cursor_y).material == Cell::WATER)
    //     return;

    cursor_x += 1;
}

void Board::move_left()
{
    // if (get_cell(cursor_x - 1, cursor_y).material == Cell::WATER)
    //     return;

    cursor_x -= 1;
}

void Board::build_city()
{
    m_cities.emplace_back(cursor_x, cursor_y);
}

void Board::print()
{
    for (auto row : m_board)
    {
        for (auto cell : row)
        {
            print_hexagon(cell.x, cell.y);
        }
    }

    print_edge(cursor_x, cursor_y);
    print_cities();
}

void Board::print_hexagon(int x, int y)
{
    const int HEXAGON_HORIZONTAL_SPACING = 6;
    const int HEXAGON_VERTICAL_SPACING = 4;

    int original_x = x, original_y = y;
    attron(COLOR_PAIR(COLOR_DEFAULT));

    // matching coordinates to 0,0 will be at the center of the board
    x += 3;
    y += 4;

    y *= HEXAGON_VERTICAL_SPACING;
    y += (x % 2) * 2;
    x *= HEXAGON_HORIZONTAL_SPACING;

    move(y + 0, x + 2);
    addstr("____");

    move(y + 1, x + 1);
    addstr("/    \\");

    move(y + 2, x);
    addstr("/ ");
    // addstr("    ");

    switch (get_cell(original_x, original_y).material)
    {
    case Cell::IRON:
        attron(COLOR_PAIR(COLOR_IRON));
        addstr("Iron");
        break;

    case Cell::WALL:
        attron(COLOR_PAIR(COLOR_WALL));
        addstr("Wall");
        break;

    case Cell::FOOD:
        attron(COLOR_PAIR(COLOR_FOOD));
        addstr("Food");
        break;

    case Cell::WOOD:
        attron(COLOR_PAIR(COLOR_WOOD));
        addstr("Wood");
        break;

    case Cell::WOOL:
        attron(COLOR_PAIR(COLOR_WOOL));
        addstr("Wool");
        break;

    case Cell::WATER:
        attron(COLOR_PAIR(COLOR_WATER));
        addstr("Aqua");
        break;
    }

    attron(COLOR_PAIR(COLOR_DEFAULT));
    addstr(" \\");

    move(y + 3, x);
    addstr("\\      /");

    move(y + 4, x + 1);
    addstr("\\____/");

    refresh();
}

void Board::print_edge(int x, int y)
{
    int original_x = x;
    x *= 3;
    y *= 4;
    // if (x <= 4)
    //     x -= (y % 2); // * 2;
    // else
    //     x -= (y % 2 == 0); // * 2;

    y += (int)(original_x / 2) * 2 % 4;

    // offset x to start after the water cells
    x += 8;
    y += 14;

    attron(COLOR_PAIR(COLOR_HOVERED));

    if (original_x % 2)
    {

        move(y, x);
        addch('_');
        addch('/');
        move(y + 1, x + 1);
        addch('\\');
    }
    else
    {
        move(y, x - 1);
        addch('\\');
        addch('_');

        move(y + 1, x - 1);
        addch('/');
    }
    attron(COLOR_PAIR(COLOR_DEFAULT));
    refresh();
}

void Board::print_cities()
{
    for (auto city : m_cities)
    {
        print_edge(city.first, city.second);
    }
}

Cell &Board::get_cell(int x, int y)
{
    if (x == 0)
        y++;
    else if (abs(x) == 3)
        y--;

    x += 3;
    y += 2;

    return m_board[x][y];
}
