#ifndef CELL_H
#define CELL_H

#include <string>

struct Cell
{
    enum Type
    {
        IRON,
        WALL,
        WOOD,
        FOOD,
        WOOL,
        WATER
    };

    Cell(int x, int y, Type material);

    int x, y;
    Type material;
};

#endif
