#ifndef BOARD_H
#define BOARD_H

#include <ncurses.h>
#include <array>
#include <vector>
#include "cell/cell.h"

class Board
{
public:
    Board();
    void init_colors();

    void print();
    Cell &get_cell(int x, int y);
    void move_up();
    void move_down();
    void move_right();
    void move_left();

    void build_city();

private:
    enum color
    {
        COLOR_HOVERED = 1,
        COLOR_DEFAULT,

        COLOR_IRON,
        COLOR_WALL,
        COLOR_FOOD,
        COLOR_WOOD,
        COLOR_WOOL,

        COLOR_WATER
    };

    void print_hexagon(int x, int y);
    void print_edge(int x, int y);
    void print_cities();

    int cursor_x, cursor_y;
    std::array<std::vector<Cell>, 7> m_board;
    std::array<std::vector<std::pair<int, int>>, 6> m_edges;
    std::vector<std::pair<int, int>> m_cities;
};

#endif
